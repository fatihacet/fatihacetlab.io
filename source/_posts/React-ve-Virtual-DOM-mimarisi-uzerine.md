---
title: React ve Virtual DOM mimarisi uzerine
tags: |-

  - ReactJS
  - Virtual DOM
permalink: react-ve-virtual-dom-mimarisi-uzerine
id: 4
updated: '2014-08-10 00:17:13'
date: 2014-08-09 23:49:27
---

React, Facebook tarafindan gelistirilen kullanici arabirimlerini olusturmayi kolaylastiran bir JavaScript library'sidir. 

> React direk DOM uzerinde degisiklik yapmak yerine Virtual DOM uzerinde degisiklikleri yapiyor.

Yukaridaki cumleyi bir cok yerde okuyup bir cok kisiden duymussunuzdur. Bu yazida React nedir, React ile bir uygulama nasil yapiliri anlatmayacagim onun yerine baslikta belirttigim gibi mimari ve Virtual DOM uzerine yaptigim arastirmalarin bir ozetini yazacagim.

DOM ile ugrasmak, DOM'a surekli yeni element'ler ekleyip cikarmak oldukca masrafli bir is. Aslinda DOM'daki bir element'in CSS class'ini bile degistirmek butun [DOM agacinin tekrar uzerinden gecilmesine](http://www.stubbornella.org/content/2009/03/27/reflows-repaints-css-performance-making-your-javascript-slow/) neden oluyor. Bu yuzden React bu tur islerin tamamini gercek DOM uzerinde degilde gercek DOM'un bir yansimasi olan sanal bir DOM uzerinde yapiyor.

React DOM islemleri icin ustun performans vaad ediyor. Bunun icin React'de belli bir kac trick mevcut.

- Virtual DOM'a yaptiginiz her islemi React direk gercek DOM'a uygulamiyor. Oncesinde bir sure bekleyerek butun degisiklikleri tek seferde uyguluyor ki yukardaki linkte anlatilan Repaint ve Reflow olayini en aza indirerek perfomansi arttiriyor.
- Gercek DOM uzerinde yapmak istediginiz degisiklikler icin gerekli olan minimum adimlari hesaplayarak minimum sayida islem yapiyor. Virtual DOM diff algoritmasi hakkinda detayli bilgi icin [su yaziya]((http://calendar.perfplanet.com/2013/diff/)) bakabilirsiniz.
- React'in view'lari tekrar render etmesi icin view'in state'inin degismesi gerekiyor ki bu da ancak `setState` method'u cagrildigi zaman gerceklesiyor. Bunun ne gibi bir artisi var sorusunun cevabi ise React data'nin degistigini anlamak icin AngularJS gibi dirty-checking ya da data object'ini surekli dinlemek gibi islemler yapmak zorunda kalmiyor. View'i tekrar render etmek icin `forceUpdate` methodunu da kullanabilirsiniz yada parent view'lardan biri tekrar render edildigi zaman React view'inizi tekrar render edecektir.

React bir View library'sidir. Yani MVC'deki V'yi olusturur. React'i istediginiz Model ve Controller ile kullanabilirsiniz ki bu da hos bir absctraction olmus. React'in mimarisi React Component'leri uzerine kurulu ve bu component'ler Virtual DOM'unuzu olusturuyor. Virtual DOM uzerinde yapacaginiz butun islemleri bu component'ler sayesinde yapiyorsunuz. Fakat bazi durumlarda gercek DOM'a da dokunmaniz gerekebilir. Ne zaman ve nasil olmasi gerektigi [su yazida](http://facebook.github.io/react/docs/working-with-the-browser.html) anlatiliyor. 

React component'leri ile ilgili son olarak eklemek istedigim sey uygulamanizin herhangi bir aninda component'inizin state'i degisirse React o component'i tekrar render ediyor. Buradaki performansi yukarida bahsettigimiz Virtual DOM diff algoritmasi ile cozuyorlar ve bu yonetimin yarari view en son data ile tekrar render edildigi icin herhangi bir state kaybi soz konusu olmuyor.

Yazimi React ile ilgili su guzel JSConf EU 2013 videosu ile bitireyim.
<iframe width="700" height="400" src="//www.youtube.com/embed/x7cQ3mrcKaY" frameborder="0" allowfullscreen></iframe>