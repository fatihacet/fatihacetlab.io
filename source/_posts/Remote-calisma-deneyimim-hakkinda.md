---
title: Remote calisma deneyimim hakkinda
tags: |-

  - remote calismak
  - remote
permalink: remote-calisma-deneyimi
id: 5
updated: '2014-10-05 19:12:30'
date: 2014-10-05 01:58:25
---

Populer kulturun kurbani olup bir [ask.fm hesabi](http://ask.fm/acetfatih) actim. Acarken biraz tereddutluydum ama sonra gelen [su guzel soru](http://ask.fm/acetfatih/answer/118339467477) dogru bir karar verdigimi anlamama yardimci oldu.

Soru `Remote calismak zor olmuyor mu? Nasil sagladin o disiplini?` idi. Bunun icin biraz uzun sayilacak bir cevap yazdim. 1 yildan fazla suredir remote calisiyorum ve bu deneyimimi paylasmak istedim. Bu blog post'umda da o cevabimi daha cok insanla paylasmak istedim.

---

> Remote calismak zor olmuyor mu? Nasil sagladin o disiplini?

Remote calismak gercekten zor. Dedigin gibi disiplin gerektirir. O disiplini saglamak zor is. Herkes remote calismayi yapamayabilir. Sevmek lazim. Ben remote calismayi seviyorum. Deneyimlerimi listelemeye calisayim.

- Evimizin bir odasi sadece calisma odasi olarak kulllaniyoruz. 3 metre beyaz IKEA masam, 90cm beyaz yazi tahtam falan var. Baya bildigin ofis gibi. Esimin de bu isi yapiyor olmasinin evin bir odasini bu sekilde kullanmaya anlasmakta cok buyuk etkisi var tabi ki. Aksi halde biraz daha zor olabilirdi. Eger remote calismak zorundaysiniz bu sekilde bir odaya ihtiyaciniz var. 5 saat bilgisayarin basindan kalkmadan calisabileceginiz bir yer lazim. Salonda televizyonun basinda, mutfakta yemek masasinin uzerinde olmaz bu is.
- Calisma saatlerinizin duzeni olmasi lazim. Belli bir saatte calismaya baslayip belli bir sure calismaniz lazim. Bir gun, iki gun gunde 3 saat calisabilirsiniz. Insanin her gunu ayni olmaz. Ama bir hafta boyunca gunde 3 saat calisamazsiniz.
- Gunde 10 saatten fazla da calismayin. Disari cikip dolasin. Kendinize vakit ayirin. Remote calismak sizi surekli evde tutmasin. Arada bilgisayarinizi alip bir cafe'de calisin.
- Zorunda olmasaniz bile kendiniz icin time tracking app'leri kullanin. Bu sizin gunde kac saat calistiginizi ogrenmenize ve icinizin rahat olmasina ya da olmamasina neden olacak. Hatta yapabiliyorsaniz oDesk gibi zalim bir sistem kullanin. oDesk remote calisma disiplini kazanmaniza yardimci olabilir.
- Yapacaginiz islerin listesi belli olsun. Uzerinde calistiginiz isin detaylarina hakim olmaniz lazim. Ne yapacaginizi bilmiyorsaniz kaybettiginiz zaman cok fazla olacaktir.
- Remote calisan olarak takim arkadaslariniza "bu adam remote calisiyor ama takima faydali" diye dusundurmek zorundasiniz. Bunun en basarili ve kolay yolu, surekli deliver etmek. Ise baslayin, bitirin ve ben bunu yaptim diyin. Bitirdiginiz is cok ufak degilse, takima mail atarak, screenshot veya screencast ile yaptiginiz isi gosterin.
- Surekli takim ile iletisim icinde olmaniz gerekiyor. Eger kucuk bir takimsaniz calismak zorunda olmadiginiz arkadasinizin bile hatrini sorun, yardimci olabileceginiz bir sey varsa yapmaya calisin. Remote olmaniz sadece sizin isinizden sorumlu oldugunuz anlamina gelmiyor.
- Ve en onemlisi aliginiz paranin hakkini verdiginizden emin olun. Bu konuda iciniz rahatsa dogru yoldasinizdir ve istediginiz sirketle remote calisabilirsiniz.
Remote calismak zor ama guzeldir. Keyif almaniz onemlidir, kendinize zaman ayirmaniz onemlidir. Aksi halde yaptiginiz isten sogursunuz ve hayatinizin ve belki de yaptiginiz isin zevki kalmaz.

Biraz uzun bir cevap oldu. Umarim faydali olmustur.

Yazinin orjinalini [surada](http://ask.fm/acetfatih/answer/118339467477) bulabilirsiniz. [ask.fm](http://ask.fm/acetfatih) uzerinden sorulariniz olursa cevap vermek isterim.