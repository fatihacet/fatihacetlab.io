---
title: Evde guzel kahve keyfi - Aeropress
tags: |-

  - aeropress
  - kahve
permalink: aeropress-ile-kahve-keyfi
id: 7
updated: '2014-12-19 19:15:53'
date: 2014-12-18 17:40:17
---

[Gokmen Goksel](http://twitter.com/gokmen)'in [su](https://twitter.com/gokmen/status/545305310689116161) tweet'inden sonra kahve seven insanlari guzel ve taze kahve icmeye tesvik etmek ve Aeropress ile tanistirmak icin bu yaziyi yazmaya karar verdim.

Aslinda pek kahve seven biri degildim. Ara sira Starbucks'tan kahve icerdim onda da Chai Tea Latte veya White Chocolate Mocha siparis ederdim. San Francisco'da bulundugum donemde sevgili Fatih Arslan'la SF'in en iyi kahve mekanlarindan Blue Bottle'da bir kahve icmistik. Istanbul'a dondugumuz zaman esim Didem'le Blue Bottle kahvelerininden cok sik bahsettigimizi ve guzel kahve icince insanin aklinda kaldigini fark ettim.

Kahve diyince aklima hep [Fatih Arslan](http://twitter.com/ftharsln) gelir. Kendisi tam bir coffee geek'tir ve kahve icmeyi cok sever. SF'daki ofiste surekli elinde kahve ogutucusu ve Aeropress ile gorurdum kendisini. Bir gun aradim ve "abi Istanbul'da Aeropress ve taze kahve bulabilecegim bir yer var mi" diye sordum. Ne guzel ki [Kahve Fabrikasi](http://www.kahvefabrikasi.com/) bu isi cok guzel bir sekilde yapiyormus. Oradan bir Aeropress, Hario kahve ogutucusu ve 2 paket kahve siparisi verdim ve Blue Bottle kahveleri gibi guzel kahveleri evde icmeye basladik.

Kisacasi taze ve guzel kahve icmek istiyorsaniz Aeropress'i denemeniz lazim. Baslamak icin [Kahve Fabrikasi](http://www.kahvefabrikasi.com/) ilk adaminiz. Aeropress, kahve ogutucusu ve her zaman taze kahve bulabileceginiz guvenilir bir yer. Dun 2 paket siparis ettim bugun geldi. 250 gr. kahve 20 lira civari. 250 gr kahve ile 13-14 bardak kahve icebilirsiniz. Kahve Fabrikasi'ndaki kahveler Colombia, Costa Rica, Kenya, Ethiopia, Guatemala gibi yerlerden gelen yoresel kahveler oluyor. Aeropress Amerika'da 25 dolar civarinda olan bir alet ama malesef bizim ulkemizde iki kati. 106 lira Kahve Fabrikasi'nda. Kahve siparisi verirken isterseniz ogutulmus olarak siparis verebilirsiniz ama ben Hario Mini Mill adinda ufak bir el degirmeni aldim bu sayede kendim ogutebiliyorum. Hario Mill Amerika'da 35 dolar Turkiye'de 133 lira. 

Aeropress ile kahve yapmanin bir cok cesidi var. Benim en cok tercih ettigim Inverted methodu. Aeropress ile kahve yapimi hakkinda [videolar](http://www.youtube.com/results?q=making+aeropress+coffee), [guzel bir yazi](https://filteredgrounds.com/how-to-make-aeropress-coffee/), [ve ustalardan yontemler](http://www.brewmethods.com/).
##### Inverted yontemi
![http://take.ms/RGdDx](http://take.ms/RGdDx)

#### Duz kullanim
![http://take.ms/AqLX9](http://take.ms/AqLX9)

---
> The AEROPRESS is an entirely new way to make coffee. Water and grounds are mixed together for ten seconds. Then gentle air pressure pushes the mix through a micro-filter in 20 seconds. The total brewing time of only 30 seconds results in exceptionally smooth flavor. 

---
#### Soyle guzel bir animasyon hazirlamislar Aeropress ile ilgili.
<iframe src="//player.vimeo.com/video/77953219" width="760" height="428" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>


---
#### Aeropress kahve yapimi
<iframe src="//player.vimeo.com/video/40980282" width="760" height="428" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>


--- 
##### Kendi cektigim Aeropress fotograflarim

<iframe src="https://www.flickr.com/photos/128358561@N02/16049936665/in/set-72157649824942815/player/" width="760" height="428" frameborder="0" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>

--- 
@gokmen'in tweet'indeki Aeropress yazisi [Aeropress / Inventing The World’s Best Coffee Maker](http://stories.maker.me/aeropress-inventing-the-worlds-best-coffee-maker/) 


--- 

Aeropress ile ilgili yardimlari ve tavsiyeleri icin Fatih Arslan'a, bu blog post'a neden olan tweet icin Gokmen Goksel'e, guzel kahveleri icin Blue Bottle'a, Aeropress icin Alan Adler reise tesekkurler.
