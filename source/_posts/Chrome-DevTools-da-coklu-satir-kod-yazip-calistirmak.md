---
title: Chrome DevTools'da coklu satir kod yazip calistirmak
tags: |-

  - chrome
  - dev-tools
  - snippets
permalink: chrome-dev-tools-snippets
id: 13
updated: '2015-07-06 05:42:51'
date: 2015-07-06 05:42:51
---

Bildiginiz gibi Chrome Dev Tools'daki `Console` tab'inde yeni satir eklemek icin `Shift+Enter` ve kodu calistirmak icin `Enter` yapmak gerekiyor. Aslinda bunun tam tersi seklinde olsa hayat benim icin daha guzel olabilirdi. Cunku varsayilan textarea davrasinisi olarak yeni satiri `Enter` ile ekliyoruz keza yillar once kullandigimiz Firebug bu sekilde calisiyordu. 

Chrome'da bunun ters oldugunu bilmeme ragmen Console'a coklu satir bir sey yazip calistirmak istedigim zaman bu ozellik hep ayagima takilip duruyordu. Bunu tersine cevirmeyi yillardan beri arar dururum ama bugun denk geldigim bir Dev Tools ozelligi ile sorunuma bir cozum bulabildim. 


#### Chrome Dev Tools Snippets

Chrome icinde yer alan `Sources` tab'indeki `Snippets` alt tabini kullanarak yeni bir snippet ekleyerek burada coklu satir JavaScript yazabiliyorsunuz ve `Command+Enter` ile calistirabiliyorsunuz. 

Snippets tab'inde sag tiklayarak yeni bir snippet olusturduktan sonra istedigimiz gibi coklu satir yazip calistirabiliyoruz.

![http://monosnap.com/image/5VGmDpsBvRLQ0rEKc642y7xy30f1vR.png](http://monosnap.com/image/5VGmDpsBvRLQ0rEKc642y7xy30f1vR.png)


Ayrica autocomplete ve syntax highlighting de calisiyor.

![http://monosnap.com/image/zIR4EDksS8kmAxnMGDQlEKHUSi3tO9.png](http://monosnap.com/image/zIR4EDksS8kmAxnMGDQlEKHUSi3tO9.png)

Bir baska guzel ozellik ise burada degistirdiginiz script'in eski versiyonlari da saklaniyor.

![http://monosnap.com/image/FXMN8xC3iy2zyOwagLOGdnpK8B0AnL.png](http://monosnap.com/image/FXMN8xC3iy2zyOwagLOGdnpK8B0AnL.png)


Son olarak Snippets tab'indeyken Console'u da actigimiz zaman tadindan yenmiyor gercekten. 

![http://monosnap.com/image/tCnHKNYltwyz7ub44aGwXhi4mRGKFt.png](http://monosnap.com/image/tCnHKNYltwyz7ub44aGwXhi4mRGKFt.png)

Benim gibi arayip bulamayanlara gelsin efenim.

BONUS: [bgrins/devtools-snippets](https://github.com/bgrins/devtools-snippets)