---
title: Turkce VueJS Egitim Serisi
tags: |-
  - vuejs
  - youtube
permalink: vuejs-videolari
id: 20
date: 2017-02-23 21:57:00
---

YouTube kanalimda yayinladigim VueJS egitimleri serisini bu yazida topluyorum. Videolarda yazilan kodlara [GitHub](https://github.com/fatihacet/vuejs-videolari) ve [GitLab](https://gitlab.com/fatihacet/vuejs-videolari)'de ulasabilirsiniz.

[YouTube Kanalim](https://www.youtube.com/channel/UCvANtNYHe556zUWm6VzJenQ)'da VueJS videolari ve Frontend gelistirme sureclerine ait videolar bulabilirsiniz.

Kanalima abone olarak yeni videolardan haberdar olabilirsiniz.
<!-- more -->
<script src="https://apis.google.com/js/platform.js"></script>

<div class="g-ytsubscribe" data-channelid="UCvANtNYHe556zUWm6VzJenQ" data-layout="full" data-count="default"></div>

---

### YouTube Playlist'i
<iframe width="1050" height="565" src="https://www.youtube.com/embed/videoseries?list=PLa3NvhdFWNipwk1KXeUpVQnAiAfuBw4El" frameborder="0" allowfullscreen></iframe>

---
#### VueJS 01: VueJS ile Uygulama Geliştirme Sunumu
<iframe width="1050" height="565" src="https://www.youtube.com/embed/byuboztKw9E" frameborder="0" allowfullscreen></iframe>

---
#### VueJS 02: Hello World!
<iframe width="1050" height="565" src="https://www.youtube.com/embed/Zkes5mS8n40" frameborder="0" allowfullscreen></iframe>

---
#### VueJS 03: v-for ile Amazon benzeri bir shopping cart yapiyoruz
<iframe width="1050" height="565" src="https://www.youtube.com/embed/kNmZAbgLkdw" frameborder="0" allowfullscreen></iframe>

---
#### VueJS 04: Computed Properties ile dinamik data kullanimi
<iframe width="1050" height="565" src="https://www.youtube.com/embed/hsrAukdWj8Q" frameborder="0" allowfullscreen></iframe>

---
#### VueJS 05: Components
<iframe width="1050" height="565" src="https://www.youtube.com/embed/LWM_7DpsnW4" frameborder="0" allowfullscreen></iframe>

---
#### VueJS 06: vue-cli ve .vue dosyalarinin yapisi
<iframe width="1050" height="565" src="https://www.youtube.com/embed/Y8pVruMaB9c" frameborder="0" allowfullscreen></iframe>

---
#### VueJS 07: Giphy API kullanarak yeni bir app yapalim
<iframe width="1050" height="565" src="https://www.youtube.com/embed/KWG4TbRXU8I" frameborder="0" allowfullscreen></iframe>

---
#### VueJS 08: Vue Router
<iframe width="1050" height="565" src="https://www.youtube.com/embed/1Z7WfxiDTjk" frameborder="0" allowfullscreen></iframe>

---
#### VueJS 09: BONUS 1: Eksik kalan kisimlar
<iframe width="1050" height="565" src="https://www.youtube.com/embed/V0wQxkaLpmk" frameborder="0" allowfullscreen></iframe>

---
#### VueJS 10: Vue JS icin Unit ve E2E testler yazmak
<iframe width="1050" height="565" src="https://www.youtube.com/embed/Ym4uH9kZEDc" frameborder="0" allowfullscreen></iframe>

---
#### VueJS 11: Swagger ile API tasarlayalim
<iframe width="1050" height="565" src="https://www.youtube.com/embed/i1ZyC0N-4Z8" frameborder="0" allowfullscreen></iframe>
