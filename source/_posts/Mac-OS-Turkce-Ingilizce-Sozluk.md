---
title: Mac OS Turkce Ingilizce Sozluk
date: 2017-01-27 00:56:25
permalink: mac-os-turkce-ingilizce-sozluk
id: 19
tags:
---

Mac OS'da bir sozcuk secip 3 parmak ile tikladiginiz zaman Look up penceresi acilacak ve size o kelimenin Ingilizce anlamini gosterecek. Soyle bir sey olmasi gerekiyor.

![http://f.acet.me/lhoA.png](http://f.acet.me/lhoA.png)

Buraya Ingilizce Turkce sozluk eklemek icin bir arastirma yaptim ve [su guzel siteye](http://clasquin-johnson.co.za/michel/mac-os-x-dictionaries/asian-languages/turkish.html) denk geldim. [Buradan](http://clasquin-johnson.co.za/michel/mac-os-x-dictionaries/asian-languages/turkish.html) veya [buradan](https://f.acet.me/5oz4.zip) Turkce-Ingilizce ve Ingilizce-Turkce sozlukleri indirip [surada](http://clasquin-johnson.co.za/michel/mac-os-x-dictionaries/) anlatildigi gibi yukleyebilirsiniz. Aslinda tek yapmaniz gereken indirdiginiz `eng-tur.dictionary` ve `tur-eng.dictionary` dosyalarini `/Library/Dictionaries` altina kopyalamak. `/Library` altinda `Dictionaries` diye bir klasor yoksa kendiniz olusturabilirsiniz.

Bunu yaptiktan sonra Mac OS'daki `Dictionary` app'ini acip Preferences'a gidin. En asagida English-Turkce ve Turkce-English diye sozlukler goreceksiz. Onlari en yukara tasiyip checkbox'larini da tiklayin.

![http://f.acet.me/Xp9Q.png](http://f.acet.me/Xp9Q.png)

Ondan sonra asagidaki gibi guzel guzel kullanabilirsiniz.

![http://f.acet.me/uwvn.png](http://f.acet.me/uwvn.png)

-------

Not: Bu yaziyi sadece daha fazla Turk kullaniciya ulasmasi ve Ingilizce ogrenmeye katkida bulunmak icin yazdim. Yani ben bu yaziyi yazmak disinda bir caba sarfetmedim, butun haklar yukarda sitesini verdigim amcaya aittir. Yukaridaki linklerden biri benim kendi S3 sunucuma giden bir link. Olurda ilerde bir gun amcanin sitesi calismazsa diye yedegini alip kendi S3'me attim.
