---
title: Cloudflare Workers Serverless Rendering
tags: |-
  - serverless
  - cloudflare
  - superpeer
permalink: serverless-rendering
id: 21
updated: '2021-02-19 10:21:54'
date: 2021-02-19 10:21:54
---

- [Tweet](https://twitter.com/i/status/1361651134037909506) by [@fatihacet](https://twitter.com/fatihacet) on [[February 16th, 2021]]:

- 🧵 Serverless ortamda SSR ve Prerendering’e bir cozum ararken, gorece yeni bir yaklasim olarak Serverless Rendering yaptik. Teknoloji olarak cok kafa acan ve bir cok ekstra olanak saglayan bu cozum hakkinda bir thread.

- 🤙 [@superpeer](https://twitter.com/superpeer) bildiginiz uzere VueJS ile gelistirdigimiz ve serverless bir mimariyle yonettigimiz ve SEO tarafi cok kritik olan bir urun. En baslarda Firebase Hosting kullaniyorduk fakat prerendering destegi vermedigi icin Netlify’a tasindik.

- 🙄 Netlify varsayilan olarak prerendering yapiyor. Sanki kendi server’inizda Nginx’le prerendering yapiyormus gibi calisiyor. Bir noktaya kadar isimizi gordu ama prerendering ayarlarina ve cache’lerine mudahale etmenize izin vermiyor. Netlify’i da hic sevmem, nedenine deginiriz :)

- 💁 Peki neden SSR yapmadik? Sirf bu sorunu cozmek icin Nuxt gibi bir yapiya gecmek ve onca refactor yapmak bana hic akil kari gelmiyordu, hala da gelmiyor. Ayrica bu paradigma degisimini ekipteki 20+ developer’a da anlatmamiz ve ogretmemiz gerekiyor.

- 😍 Alternatiflere bakarken 3 ay oncesinde Cloudflare Workers denemistim ve alete direk asik oldum. POC’yi yapip kenara koymustum ama baska islerin yogunlugundan bitirememistim. [Serverless Rendering with Cloudflare Workers](https://blog.cloudflare.com/serverless-rendering-with-cloudflare-workers/)

- 😱 Cloudflare Workers’in yaptigi is tarayicidan gelen istege index.html’i donmeden once araya girip icerigi degistirmenize ya da baska bir icerik donmenize izin veren, Service Worker’lara benzeyen bir alet.

- 🚀 Cloudflare Workers bu islemleri network edge’de daha request’e cevap donmeden yapip, KV isminde bir in-memory-cache’i kullanmamiza izin verdigi icin asiri hizli calisiyor. Ayrica Cloudflare kullaniyorsunuz, dunyanin her yerinde CDN’leri olan bir infra, mis gibi 🤩

- 🥰 Bu cozum prerendering gibi eger gelen kisi search bot’sa bu icerigi don, degilse index.html’i don sekilde “hacky” de degil. Ayrica tek olayi da bu degil. A/B test, URL rewrite, redirect, header degistirme, country check gibi tonla seye izin veriyor. [Examples · Cloudflare Workers docs](https://developers.cloudflare.com/workers/examples)

- 💪 Dun gece [@akngundogdu](https://twitter.com/akngundogdu) [@moersoy](https://twitter.com/moersoy) ve [@abdullahmara00](https://twitter.com/abdullahmara00)’yla oturup oncesinde yaptigim POC’yi production ready hale getirdik ve sanirim 2 hafta icinde yayina aliriz. Netlify’dan ciktigimiz icin de ayrica sevincliyim 🙌

- ⚡️ Teknik olarak, once oturup bizim Cloud Run uzerine bir tane endpoint yazdik. Yaptigi tek is, verdigimiz URL’e ait meta tag’leri rewrite etmek icin bir kac key/value data ve sayfanin icine koymak icin statik bir HTML donmek.

- 🤘 Sonrasinda cok kolay ve hizli bir sekilde yeni bir Cloudflare Worker projesi ayaga kaldirdik ve Cloudflare Worker Sites ile deploy ettik. Bunu yaptigimizda bize ait bir domain’de Superpeer Staging ortamini preview edebilir olduk ama hala serverless rendering kismina gelmedik.

- ✌️ Cloudflare Worker size worker.js isminde bir JS file ve bazi rewrite API’lari veriyor. Bu worker kodu icerisinde once bizim Cloud Run’a istek yapip JSON data aldiktan sonra bunu index.html ekleyip ve bir kac rewrite isleminden sonra client’a donduk. Her sey cok da guzel oldu.

- 😒 Tabi ki bu kadar kolay ve hizli olmadi. Dun en az 6 saat bunun uzerinde calistik. Rewrite kismi gereginden fazla zorladi. SSL handshake’le baya ugrastik. Dokumantasyon iyi ama daha cok ornek super olurdu. Debug etmesi de o kadar kolay degildi ama gunun sonunda ❤️

- 🙌 En cok hosuma giden seyse Netlify’dan cikip dogru duzgun cache ve invalidation yapacak Cloudflare’e gecis yapmis olmak ve bunu hic bir developer’in workflow’unu degistirmeden arka tarafta arastirma kismi haric 1 gunde yapabilmis olmak. Nuxt ile en az 2 hafta gerekirdi.
