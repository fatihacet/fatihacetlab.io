---
title: "Frontend Calisma Grubu"
tags: |-
  - frontend-calisma-grubu
  - fcg
permalink: frontend-calisma-grubu
date: 2019-11-01 15:03:00
---

[Cok uzun zamandan](https://gist.github.com/fatihacet/ad80581c127a30504f4ca67f2141b57e) beri hayalim olan bir fikri Frontend Calisma Grubu adi altinda hayata gecirmeye kadar verdim. Frontend Calisma Grubu benim insiyatifini aldigim gonullu bir girisim. Amacim 6 tane genci sektoru kazandirmak. YouTube videolarimin altina gelen yorumlardan gordugum uzere bir cok insanin hayatina dokunmanin gururunu yasadim. Simdi bunu ufak bir grupla baslayarak daha duzenli bir sekilde yapmak istiyorum.

---

#### Nedir?
[Cok uzun zamandan](https://gist.github.com/fatihacet/ad80581c127a30504f4ca67f2141b57e) beri hayalim olan bir fikri Frontend Calisma Grubu adi altinda hayata gecirmeye kadar verdim. Frontend Calisma Grubu benim insiyatifini aldigim gonullu bir girisim. Amacim 6 tane genci sektoru kazandirmak. YouTube videolarimin altina gelen yorumlardan gordugum uzere bir cok insanin hayatina dokunmanin gururunu yasadim. Simdi bunu ufak bir grupla baslayarak daha duzenli bir sekilde yapmak istiyorum.


#### Peki neden simdi?
Acikcasi kardesimin JavaScript bilgisini daha fazla gelistirmesine yardimci olmak suan icin en buyuk motivasyonum. Bunu yaparken ona ayiracagim zamanin baska kisilerin gelisimine de katki saglayabilecegini dusundugum icin uzun zamandir bekleyen bu fikri hayata gecirme karari aldim.


#### Kac kisi olacak?
Toplamda 6 kisi olacak. Bu sayede 2 kisilik 3 grup halinde pair-programming, pair code review gibi calismalar yapabiliriz.


#### Ne zaman baslayacak ve ne kadar surecek?
4 Kasim Pazartesi baslayacak ve tahminen 2 ay surecek.


#### Kimler basvurabilir?
Yas, okul, deneyim gibi bir kisitlama dusunmuyorum. Fakat 6 kisiden 4unun Junior, 2sinin Junior'dan hallice olmasi gibi bir hayalim var. Cunku calisma grubunun, ilerleyen zamanlarda, olabildigince kendi kendine devam edebilmesi bu iki arkadasin bayragi tasinamasi gerekiyor.


#### Calisma plani nedir?
HTML ve CSS sonrasinda JavaScript. Suan icin detayli bir ders plani yok. Fakat kabaca [UI8](https://ui8.net)'deki bir template'i alip HTML'ini ve CSS'ini yazarak baslayacagiz. Eger grubun HTML/CSS kisminda eksigi varsa oncesinde bunu tamamlamak ve sonrasinda UI8 template'inin bitirilip, pair-review edilmesi. Daha sonrasinda JS kismina gecip, [Nicholas Zakas](https://humanwhocodes.com/)'in [Professional JavaScript for Web Developers](https://www.amazon.com/Professional-JavaScript-Developers-Nicholas-Zakas-ebook/dp/B006PW2URI) kitabini kilavuz alacagiz. Calisma planini baslamadan once detaylandirip ve calisma grubuyla tartisacagiz.


#### Calisma grubu nasil ilerleyecek?
En buyuk hayalim, grubun kendi kendine ilerleyebilmesi ve hatta ilerleyen zamanlarda baska gruplar olusturup onlara mentorluk vermesi. Bu noktaya gelebilirsek muazzam bir is basarmis oluruz ama once bir baslayip, calistigini gormemiz lazim.

Bu grup icin en buyuk yuk bende olacak ama gruptaki herkesin proaktif olarak surukli iletisim halinde kalip benden rol calmasini istiyorum. Bu is bir kisinin, _benim_, tek basina altina girip saatlerini harcayarak surekli konu anlatmasi seklinde yuruyemez. O yuzden grup uyelerinin de sorumluluk almasi, arastirmasi ve paylasmasi gerekiyor.


#### Ne ogrenecegim? Ya bir sey ogrenemezsem?
Acikcasi baya bir seyler ogrenecegini dusunuyorum ama bu konuda soz veremem. Bu iyi bir niyet icin ilk adim ve tamamen gonullu bir hareket. Umarim yararli olabilirim ama benden once grup icindekilerin kendilerine yararli olmasi lazim. Eger senin icin yurumedigini, ise yaramadigini dusundugun an ayrilabilirsin.


#### Grup ici iletisim nasil olacak?
Calisma grubu icin bir Slack acmayi ve bu 6 kisiyi davet edip baslamayi dusunuyorum. Online meeting'ler icin Zoom kullanacagiz. Hatta maksimum faydayi saglayabilmek icin Zoom meeting'lerini kaydetip [YouTube kanalimda](https://youtube.com/fatihacet) yayinlayabiliriz.


#### Gereksinimler nedir?
- Zaman ayiracagina ve calisacagina dair soz vermek
- Zoom toplantilarina duzenli olarak katilmak
- Verilen assignment'lari zamaninda yapmak
- Slack'te aktif olup grub icindekilere yardim etmek


#### Nasil geri bildirimde bulunabilirim?
✉️ [fcg@fatihacet.com](mailto:fck@fatihacet.com?Subject=Frontend%20Calisma%20Grubu)


#### Ben de mentor olabilir miyim?
Cok sevinirim. Calisma grubuna bir saatini ayirip kariyer gelisimi, Frontend Gelistiricilerin sorumluluklari, is hayati, maas konusmalari gibi konular yaninda CI/CD, kod review surecleri gibi teknik konularda konusma yapmak isteyenler ❤️ ✉️ [fcg@fatihacet.com](mailto:fck@fatihacet.com?Subject=Frontend%20Calisma%20Grubu)


#### Nasil basvuruda bulunuyorum?
[Frontend Calisma Grubu Basvuru Formu](https://forms.gle/WbLzo6qvjnFg398V8)
