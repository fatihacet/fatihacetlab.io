---
title: Blogu Hexo ve GitLab Pages'a tasidim
tags: |-

  - hexo
  - blog
  - gitlab
permalink: hexo-gitlab-blog
id: 18
date: '2017-01-23 22:04:00'
---

5 Agustos 2014 tarihinde DigitalOcean'daki bir makineye Cihangir Savas ile beraber Ghost blog kurmustuk. O zamandan bu zamana kadar, ayni DO makinesindeki Ghost blog sikintisiz bir sekilde calismaya devam etti. Ozellikle son 1, 1.5 yildir DO makineme nedendir bilmiyorum ama SSH yapamiyordum. Yani blog down olsa, ya da update etmek istesem zaten edemiyordum.


Gecen haftasonu can sikintisindan blog'u [Hexo](https://github.com/hexojs/hexo) ile yeniden olusturup GitLab Pages uzerinde serve etmeye basladim. Hexo nereden cikti derseniz aslinda son anda aklima dustu. Soyle bir baktim guzel gozuktu. Hexo temalarina baktim [istedigim gibi bir tema](https://github.com/sjaakvandenberg/flexy) bulunca bir sans vereyim dedim.


Ghost blog'dan butun datayi JSON olarak export ettim ve [hexo-migrator-ghost](https://github.com/jasonslyvia/hexo-migrator-ghost) kullanarak Ghost JSON'i Hexo'ya uygun hale getirdim. Benim Ghost cok eski oldugu icin hexo-migrator-ghost calismadi. Kodunu debug edip, kendim icin calisir hale getirdim ve [bir de Pull Request](https://github.com/jasonslyvia/hexo-migrator-ghost/pull/2) gonderdim.


Hexo'yu local'de istedigim gibi ayarladiktan sonra [GitLab'da bir repo](https://gitlab.com/fatihacet/fatihacet.gitlab.io) actim ve GitLab Pages ile statik blog'u yayinlamak icin [soyle basit bir GitLab CI yaml](https://gitlab.com/fatihacet/fatihacet.gitlab.io/blob/master/.gitlab-ci.yml) ekledim.


Bunu yaptiktan sonra GitLab benim icin blog'umu olusturacak ve [fatihacet.gitlab.io](fatihacet.gitlab.io) domain'inde blog'u yayinlamaya baslayacakti. GitLab repo ayarlarindaki Pages sekmesi altinda kendi domain'im icin [fatihacet.com](fatihacet.com) ozel domain ekledim ve domain'im CloudFlare'dan yonettigim icin CloudFlare'da son olarak GitLab Pages URL'i icin bir A Record ekledim ve blog'um hazirdi.


Yorum sistemi olarak zaten Ghost Blog'da da Disqus kullaniyordum Hexo icin de [Disqus](disqus.com) kullanmaya devam ettim. Zaten statik blog uygulamalarinda database falan olmadigi icin Disqus gibi araclari kullanmaniz gerekiyor. Eski yorumlarin gozukmesi icin tek yapmam gereken domain'i ve blog post link'lerini eskisi gibi birakmakti.


Hexo + GitLab Pages'in bir cok guzel ozelligi var, mesela

- Bir server'la ugrasmak ve para odemek zorunda degilim.
- Hexo'nun en son versiyonuna yukseltmek icin ugrasmak zorunda degilim.
- Yeni bir blog post yayinlamak icin `git push` yapmam yeterli.
- Blog direk `https` uzerinden yayinlaniyor.

Bakalim bu blog sistemi ile ne kadar devam edecegim.
