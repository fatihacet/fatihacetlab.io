const fs = require('fs');
const request = require('request');
const Vue = require('vue');
const VueTimeago = require('vue-timeago');
const renderer = require('vue-server-renderer').createRenderer();
const entities = require('entities');

Vue.use(VueTimeago, {
  name: 'timeago',
  locale: 'tr-TR',
  locales: {
    'tr-TR': require('./locales/tr-TR.json')
  }
});

const YouTubeVideo = {
  props: {
    video: {
      type: Object,
      required: true,
    },
  },
  computed: {
    snippet() {
      return this.video.snippet;
    },
    coverPath() {
      return this.snippet.thumbnails.medium.url;
    },
    videoUrl() {
      return `https://www.youtube.com/watch?v=${this.video.id.videoId}`;
    },
    title() {
      return entities.decodeHTML(this.snippet.title);
    },
  },
  template: `
    <div class="video">
      <a :href="videoUrl" target="_blank">
        <img :src="coverPath" />
      </a>
      <h2>
        <a :href="videoUrl" target="_blank">{{ title }}</a>
        <timeago :datetime="snippet.publishedAt" />
      </h2>
      <article>{{snippet.description}}</article>
    </div>
  `
};

const vueOptions = {
  data: {
    videos: [],
  },
  components: {
    'yt-video': YouTubeVideo,
  },
  template: `
    <section>
      <yt-video
        v-if="videos.length"
        v-for="video in videos"
        :video="video"
        key="video.id.videoId" />
    </section>
  `
};


const url = `https://www.googleapis.com/youtube/v3/search?key=${process.env.YT_API_KEY}&channelId=${process.env.YT_CHANNEL_ID}&part=snippet,id&order=date&maxResults=50&type=video`;
request(url, (err, res, body) => {
  if (err) throw err;

  vueOptions.data.videos = JSON.parse(body).items;
  const app = new Vue(vueOptions);

  renderer.renderToString(app, (err, html) => {
    if (err) throw err;

    fs.writeFileSync(`${__dirname}/../themes/flexy/layout/_videos.html`, html);
  });
});
